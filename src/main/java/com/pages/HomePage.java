package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void goToTrendyol () {
        driver.navigate().to("https://www.trendyol.com/");
        waitPageLoad();
    }

    public void closeHomePagePopup() {
        handlePopup(By.cssSelector(".fancybox-item.fancybox-close"));
    }

    public void moveToLoginIcon() {
        WebElement loginIcon = findElement(By.cssSelector(".icon.icon-user"));
        moveToElement(loginIcon);
    }

    public void clickLoginButton () {
        clickBy(By.cssSelector(".account-button.login"));
    }

    public void goToBasket() {
        moveToElement(findElement(By.cssSelector("#myBasketListItem")));
        if(findElement(By.cssSelector(".goBasket")).isDisplayed()) {
            clickBy(By.cssSelector(".goBasket"));
            waitPageLoad();
            sleep(3000);
        }
    }

    public int getNavigationLinkSize () {
        return findElements(By.cssSelector("ul[class='main-nav']>li")).size();
    }

    public void clickNavigationLinks (int i) {
        waitPageLoad();
        System.out.println("Navigation-Link No: " + i);
        List <WebElement> navigationLinks = findElements(By.cssSelector("ul[class='main-nav']>li"));
        //Click navigation links
        clickElement(navigationLinks.get(i));
    }
}
