package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage{
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void loginToTrendyol () {
        write(By.id("email"), "trendyol-test@mailinator.com");
        write(By.id("password"),"trend1234");
        clickBy(By.id("loginSubmit"));
        waitVisibility(By.cssSelector(".login-container span"));
        isTextEquals(By.cssSelector(".login-container span"), "Hesabım");
    }
}
