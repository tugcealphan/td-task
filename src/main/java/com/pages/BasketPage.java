package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class BasketPage extends  BasePage {
    public BasketPage(WebDriver driver) {
        super(driver);
    }

    public void cleanBasket () {
        if (isElementExists(By.cssSelector(".removeitem"))) {
            System.out.println("Remove item exists");
            List<WebElement> basketRemoveSigns = findElements(By.cssSelector(".removeitem"));
            for (int i = 0; i < basketRemoveSigns.size(); i++) {
                System.out.println("Remove item no: " + i);
                waitPresence(By.cssSelector(".removeitem"));
                sleep(1000);
                clickBy(By.cssSelector(".removeitem"));
                sleep(500);
                clickBy(By.cssSelector("body > div.lightbox > div.lightboxbox > div > div:nth-child(3) > a:nth-child(1)"));
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[text()='Ürün sepetinizden başarıyla silindi']")));
                sleep(500);
            }
        }
    }

    public void goToHomePage () {
        clickBy(By.cssSelector("[class=\"col-lg-3 colmd-3 col-xs-3 no-padding\"] .icon-trendyol"));
        waitPageLoad();
    }
}
