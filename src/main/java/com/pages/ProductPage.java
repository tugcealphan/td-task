package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ProductPage extends BasePage{


    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void addToBasket () {
        clickBy(By.cssSelector(".add-to-basket-text"));
        waitPageLoad();
        sleep(500);
        //If size exists, select the first size
        if (isElementExists(By.cssSelector(".dropdown-menu.open"))) {
            waitVisibility(By.cssSelector(".dropdown-menu.open"));
            waitVisibility(By.cssSelector(".mCSB_container"));
            //clickBy(By.cssSelector("div>li[data-original-index='1']"));
            waitPageLoad();
            //waitVisibility(By.cssSelector("div>li[data-original-index]:not(.disabled)"));
            sleep(3000);
            waitPresence(By.cssSelector("div>li[data-original-index]:not(.disabled)"));
            List<WebElement> availableItemList = driver.findElements(By.cssSelector("div>li[data-original-index]:not(.disabled)"));
            int randomAvailableItem = randomInteger(availableItemList.size()-1,1);
            WebElement element =  wait.until(ExpectedConditions.elementToBeClickable(availableItemList.get(randomAvailableItem)));
            element.click();
            //clickElement(availableItemList.get(randomAvailableItem));
            waitPageLoad();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".dropdown-menu.open")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#basketPreviewcontent")));
            sleep(500);
            clickBy(By.cssSelector(".add-to-basket-text"));
            sleep(500);
            //moveToElement(findElement(By.cssSelector("#AutoCompleteBox")));
            clickBy(By.cssSelector("#AutoCompleteBox"));
            sleep(500);

            //clickBy(By.cssSelector(".add-to-basket-text"));
        } else {
            //moveToElement(findElement(By.cssSelector("#AutoCompleteBox")));
            clickBy(By.cssSelector("#AutoCompleteBox"));
            waitPageLoad();
            sleep(500);
        }
    }
}
