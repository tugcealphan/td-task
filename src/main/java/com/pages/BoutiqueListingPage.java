package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class BoutiqueListingPage extends BasePage {
    public BoutiqueListingPage(WebDriver driver) {
        super(driver);
    }

    public void clickRandomBoutiqueSection () {
        //Get all boutiques
        List<WebElement> boutiquesSections = findElements
                (By.cssSelector("div[class='boutique-large-list-tmpl-result row']>div"));
        if (boutiquesSections.size() <= 0) {
            System.out.println("There are not any boutiques on the page!");
        } else {
            int randomInt = randomInteger(boutiquesSections.size() - 1, 0);
            clickElement(boutiquesSections.get(randomInt));
            System.out.println("Page Title: " + driver.getTitle());
        }
    }


}
