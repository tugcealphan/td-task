package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class BasePage {

    public WebDriver driver;
    public WebDriverWait wait;
    public JavascriptExecutor jsExec;

    public BasePage (WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver,20);
        jsExec = (JavascriptExecutor) driver;
    }

    protected WebElement findElement (By by) {
        //Wait
        waitPageLoad();
        waitVisibility(by);
        return driver.findElement(by);
    }

    protected List<WebElement> findElements (By by) {
        waitPageLoad();
        return driver.findElements(by);
    }


    protected void waitPageLoad () {
        wait.until(webDriver -> (jsExec.executeScript("return document.readyState").equals("complete")));
    }

    protected void clickBy (By by) {
        //Wait
        waitPageLoad();
        waitVisibility(by);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
        //Click
        element.click();
    }

    protected void clickByJS (By by) {
        waitVisibility(by);
        WebElement elementToClick = findElement(by);
        jsExec.executeScript("arguments[0].scrollIntoView()", elementToClick);
        jsExec.executeScript("arguments[0].click();", elementToClick);
    }

    protected void clickElementJS (WebElement elementToClick) {
        wait.until(ExpectedConditions.visibilityOf(elementToClick));
        jsExec.executeScript("arguments[0].scrollIntoView()", elementToClick);
        jsExec.executeScript("arguments[0].click();", elementToClick);
    }

    protected void clickElement (WebElement element) {
        //Wait
        waitPageLoad();
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        //Click
        element.click();
    }

    protected void waitVisibility (By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    protected void waitPresence (By by) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    protected void write (By by, String text) {
        waitPageLoad();
        waitVisibility(by);
        findElement(by).sendKeys(text);
    }

    protected String getText (By by) {
        waitPageLoad();
        waitVisibility(by);
        return findElement(by).getText();
    }


    protected void sleep (long sleepTime){
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    protected void handlePopup (By by) {
        waitPageLoad();
        List<WebElement> popupList = driver.findElements(by);
        if(!popupList.isEmpty()) {
            popupList.get(0).click();
        }
    }


    protected Boolean isElementExists (By by) {
        waitPageLoad();
        List<WebElement> elementList = driver.findElements(by);
        if(!elementList.isEmpty() && elementList.get(0).isDisplayed() && elementList.get(0).isEnabled()) {
            return true;
        } else {
            return false;
        }
    }

    protected void moveToElement (WebElement element) {
        waitPageLoad();
        Actions builder = new Actions(driver);
        Action moveToElement = builder.moveToElement(element).build();
        moveToElement.perform();
    }

    protected Boolean isTextEquals (By by, String expectedText) {
        waitPageLoad();
        String text = getText(by);
        if(text.equals(expectedText)) {
            System.out.println("Texts are equal!" + "Actual: " + text + " " + "Expected: " + expectedText);
            return true;
        } else {
            System.out.println("Texts are not equal!" + "Actual: " + text + " " + "Expected: " + expectedText);
            return false;
        }
    }

    protected int randomInteger (int max, int min) {
        Random random = new Random();
        int randomNumber = random.nextInt(max-min+1) + min;
        return randomNumber;
    }



}
