package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ProductListingPage extends BasePage{
    public ProductListingPage(WebDriver driver) {
        super(driver);
    }

    public void clickRandomProduct () {
        List<WebElement> productList = findElements
                (By.cssSelector("div[class='products-container grid-4']>div>div"));
        if(productList.size()<=0){
            System.out.println("There are not any products on the page!");
        } else {
            int randomInt = randomInteger(productList.size() - 1, 0);
            clickElement(productList.get(randomInt));
            System.out.println("Page Title: " + driver.getTitle());
        }
    }
}
