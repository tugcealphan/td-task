package com.tests;

import com.pages.*;
import org.testng.annotations.Test;

public class LoginAndCheckBoutiquesTest extends BaseTest {

    @Test
    public void loginAndCheckBoutiquesTest (){
        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);
        BoutiqueListingPage boutiqueListingPage = new BoutiqueListingPage(driver);
        ProductListingPage productListingPage = new ProductListingPage(driver);
        ProductPage productPage = new ProductPage(driver);
        //BasketPage basketPage = new BasketPage(driver);

        homePage.goToTrendyol();
        homePage.closeHomePagePopup();
        homePage.moveToLoginIcon();
        homePage.clickLoginButton();
        loginPage.loginToTrendyol();
        /*homePage.goToBasket();
        basketPage.cleanBasket();
        basketPage.goToHomePage();*/

        for(int i = 0; i<homePage.getNavigationLinkSize(); i++) {
            homePage.clickNavigationLinks(i);
            boutiqueListingPage.clickRandomBoutiqueSection();
            productListingPage.clickRandomProduct();
            productPage.addToBasket();
        }

    }
}
