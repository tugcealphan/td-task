package com.tests;

import com.utilities.DriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import javax.lang.model.type.NullType;

public class BaseTest {
    public WebDriver driver;
    public DriverManager driverManager = new DriverManager();

    @BeforeMethod
    @Parameters("browser")
    public void setup (@Optional String browser) {
        if (browser == null) {
            browser = "chrome";
        }
        System.out.println("Browser: " + browser);
        //Create driver with given browser
        driver = driverManager.createDriver(browser);
    }

    @AfterMethod
    public void teardown() {
        driver.quit();
    }

}
