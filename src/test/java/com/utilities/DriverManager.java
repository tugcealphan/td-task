package com.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//This class is responsible to create driver according to given browser
public class DriverManager {

    public OptionsManager optionsManager = new OptionsManager();

    public WebDriver createDriver (String browser) {
        if (browser.contains("firefox")) {
            return new FirefoxDriver(optionsManager.getFirefoxOptions());
        } else if (browser.contains("chrome")) {
            return new ChromeDriver(optionsManager.getChromeOptions());
        } else {
            System.out.println("There is a problem in driver creation.");
            return null;
        }
    }


}
